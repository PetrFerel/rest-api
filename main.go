package main

import (
	"CoffeeAPI/MarketCoffee"
	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()
	router.GET("/coffee", MarketCoffee.GetCoffee)
	router.DELETE("/coffee/:id", MarketCoffee.DeleteCoffee)
	router.PUT("/coffee/:id", MarketCoffee.UpdateCoffee)
	router.POST("/coffee", MarketCoffee.PostCoffee)
	router.GET("/coffee/:id", MarketCoffee.GetCoffeeId)
	router.Run("localhost:8080")
}
