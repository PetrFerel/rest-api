package MarketCoffee

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type CoffeeChop struct {
	ID     string  `json:"id"`
	Brand  string  `json:"brand"`
	Marka  string  `json:"marka"`
	Price  float64 `json:"price"`
	Volume float64 `json:"volume"`
}

var coffee = []CoffeeChop{
	{ID: "1", Brand: "Starbucs", Marka: "Capuchino", Price: 1.99, Volume: 0.33},
	{ID: "2", Brand: "Cossto", Marka: "Latte", Price: 2.15, Volume: 0.5},
}

func GetCoffee(c *gin.Context) {
	c.IndentedJSON(http.StatusOK, coffee)

}

func PostCoffee(c *gin.Context) {
	var newCoffee CoffeeChop

	if err := c.BindJSON(&newCoffee); err != nil {
		return
	}
	coffee = append(coffee, newCoffee)
	c.IndentedJSON(http.StatusCreated, newCoffee)
}
func GetCoffeeId(c *gin.Context) {
	id := c.Param("id")
	for _, item := range coffee {
		if item.ID == id {
			c.IndentedJSON(http.StatusOK, item)
		}
	}
	c.IndentedJSON(http.StatusNotFound, gin.H{"message": "album is not found"})
}

func DeleteCoffee(c *gin.Context) {
	id := c.Param("id")
	for i, item := range coffee {
		if item.ID == id {
			coffee = append(coffee[:i], coffee[i+1:]...)
			c.IndentedJSON(http.StatusNoContent, item)
			return
		}

	}
	c.IndentedJSON(http.StatusNotFound, gin.H{"message": "not delite content"})

}
func UpdateCoffee(c *gin.Context) {
	id := c.Param("id")
	for i, item := range coffee {
		if item.ID == id {
			c.BindJSON(&coffee[i])
			//coffee[i] = item
			c.IndentedJSON(http.StatusOK, item)
			return
		}
	}
	c.IndentedJSON(http.StatusFound, gin.H{"messsage": "not update"})
}
